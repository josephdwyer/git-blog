/* Joseph Dwyer
 * V.001
 */

var blog = blog || {};

blog.Post = function (post_metadata) {
  function unencode(text) {
    // reverse the html encoding - do we need a more thorough approach?
    text = text.replace(/&lt;/g,"<");
    text = text.replace(/&gt;/g,">");
    text = text.replace(/&amp;/g,"&");

    return text;
  }

  function codemirrorPost($container) {
    blog.Post.code_block_id = blog.Post.code_block_id || 0;
    $(document).ready(function() {
      $container.find('code').each(function() {
        var local_block_id = blog.Post.code_block_id += 1;
        var $obj = $(this);
        var code = $obj.html();
        var lang = $obj.attr('class');
        $obj.replaceWith($("<textarea id='code" + local_block_id + "' />"));
        var editor = CodeMirror.fromTextArea(document.getElementById('code' + local_block_id), {mode: lang, theme: "blackboard", lineNumbers: true, matchBrackets: true});
        editor.setValue(unencode(code));
      });
    });
  }

  var id = post_metadata.file.replace(".", "_").replace("/", "_");
  var self = {};
  $.extend(self, 
  {
    id: id,
    meta : post_metadata,
    load: function($container) {
      // ajax in the markdown 
      blog.config.scm.getFileContents("posts/" + this.meta.file, function (contents) {
        // call render
        self.render($container, contents);
      });
    },
    render : function($container, markdown) {
      // render the markdown.
      var converter = new Showdown.converter();
      var post_html = converter.makeHtml(markdown);
      $container.html(post_html);
      codemirrorPost($container);
    }
  });
  return self;
}

blog.Bitbucket = function(user, content_repo) {
  var activeAjaxCalls = 0;
  get_from_url = function(url, output, callback) {
    $.ajax({
      url: url,
      dataType: 'jsonp',
      beforeSend: function(xhr) {
        activeAjaxCalls += 1;
      },
      error: function(xhr, status, error) {
        activeAjaxCalls -= 1;
        alert(error);
      },
      success: function(data) { 
        activeAjaxCalls -= 1;
        callback(output(data));
      },
    });
  }
  function getFolderContentsUrl(folder) {
    return "https://api.bitbucket.org/1.0/repositories/" + user + "/" + content_repo + "/src/master/" + folder + "/";
  }
  function getPostUrl(path) {
    return "https://api.bitbucket.org/1.0/repositories/" + user + "/" + content_repo + "/src/master/" + path;
  }
  return {
    getFolderContents: function(folder, callback, filter) {
      var url = getFolderContentsUrl(folder);
      get_from_url(url, function(d) { return d.files; }, function(o) {
        if(filter) {
          var rtrn = [];
          for(var i = 0; i < o.length; i++) {
            if(filter(o[i])) {
              rtrn.push(o[i]);
            }
          }
        }
        else {
          rtrn = o;
        }
        callback(rtrn);
      });
    },
    getFileContents: function(path, callback) {
      var url = getPostUrl(path);
      get_from_url(url, function(d) {return d.data;}, callback); 
    },
    hasActiveConnections: function() {
      return activeAjaxCalls > 0;
    }
  };
}

blog.config = {
  post_container: ".js-post-container",
  scm: blog.Bitbucket("josephdwyer", "git-blog-posts")
}

blog.posts = [];

blog.load = function($post_container) {
  function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
  }
  function renderMetas(meta_paths) {
    var metas = [];
    for(var i = 0; i < meta_paths.length; i++) {
      blog.config.scm.getFileContents(meta_paths[i].path, function(meta) {
        metas.push(JSON.parse(meta));

        if(!blog.config.scm.hasActiveConnections()) {
          metas.sort(function(a,b) {
           var dateA=new Date(a.date);
           var dateB=new Date(b.date);
           return dateB-dateA //sort by date descending
          });
          for(var i = 0; i < metas.length; i++) {
            var post = blog.Post(metas[i]);
            $post_container.append("<div id='" + post.id + "'></div>");
            post.load($('#'+ post.id));
          }
        }
      });
    }
  }
  blog.config.scm.getFolderContents("posts", renderMetas, function(f) { return endsWith(f.path, '.json'); }); 
}
$(document).ready(function() {
  blog.load($(blog.config.post_container));
});

